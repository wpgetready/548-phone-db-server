<?php

//Additional headers needed to javascript invokes
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

define('DS', DIRECTORY_SEPARATOR);


/*
$matricula=isset($_GET["mat"])?$_GET["mat"]:"";
if ($matricula=="") {
	echo "Invoke Error (-1)";
	die();
}
*/
//Return latest notifications list.


$key=isset($_GET["key"])?$_GET["key"]:"";
if ($key=="") {
	echo "Invoke Error(-2)";
	die();
}

require_once('classes' . DS . 'sqlite.php');
require_once('classes' . DS . 'utilities.php');


if(!utilities::checkKey($key))
{
    echo "Invoke Error (-3)";
    die();
}


$sqlite = new sqlite(); //trying to get an instance...
$result =$sqlite->get_latest_notification_JSON();
echo $result;
die();
?>