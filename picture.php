<?php
//Additional headers needed to javascript invokes
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

define('DS', DIRECTORY_SEPARATOR);

$id_plate=isset($_GET["id_plate"])?$_GET["id_plate"]:"";
if ($id_plate=="") {
	echo "Invoke Error (-1)";
	die();
}
/*
$key=isset($_GET["key"])?$_GET["key"]:"";
if ($key=="") {
	echo "Invoke Error(-2)";
	die();
}
*/
require_once('classes' . DS . 'sqlite.php');
require_once('classes' . DS . 'utilities.php');
/*

if(!utilities::checkKey($key))
{
    echo "Invoke Error (-3)";
    die();
}
*/
$sqlite = new sqlite(); //trying to get an instance...

//$result = new photoResponse(); //THIS INITIALIZATION IS USED ONLY FOR EASING edition. DELETE AFTER!
$result =$sqlite->return_pictures($id_plate); //No images generated
echo($result);
?>