<?php
//Additional headers needed to javascript invokes
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");
error_reporting(-1);
define('DS', DIRECTORY_SEPARATOR);
$test=isset($_GET["test"])?$_GET["test"]:"";


$diagramId=isset($_GET["diagramid"])?$_GET["diagramid"]:"";
$phoneId=isset($_GET["phoneid"])?$_GET["phoneid"]:"";
$search=isset($_GET["search"])?$_GET["search"]:"";

if ($phoneId=="" && $search=="" && $diagramId=="") {
        echo "Invoke Error phoneid ,search and diagram empty(-1)";
        die();
}

//ECHO "AGREGAR DE NUEVO LAS CLAVES!!!!!<br>";

if ($test!="1") {
    $key=isset($_GET["key"])?$_GET["key"]:"";
    if ($key=="") {
        echo "Invoke Error(-2)";
        die();
    }
}

require_once('classes' . DS . 'sqlite.php');
require_once('classes' . DS . 'utilities.php');

if($test!="1") {
    if(!utilities::checkKey($key))
    {
        echo "Invoke Error (-3)";
        die();
    }
}

$sqlite = new sqlite(); //trying to get an instance...
//$result =$sqlite->getPhone($idphone);

if ($search!=""){
    $result =$sqlite->getSearchPhone($search);
    echo $result;
    die();
}

if ($phoneId!="") {
    $result =$sqlite->getPhoneDiagrams($phoneId);
    echo $result;
    die();
}

if($diagramId!="") {
    $result =$sqlite->requestObject($diagramId);
    echo $result;
    die();

}

?>