<?php

class utilities
{

    const normalDictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    const juggledDictionary = "A0B1D2E3F4G5D6H7I8J9KLMNOPQRSTUVWXYZ";


    /**
     * Since it is possible service has some time deviation, I check code with today and yesterday
     * 20170406: A sepcial case: users ahead of time can't use the app, so I added 12 hours from current time.
     * @param $key
     * @return bool
     */
    static function checkKey($key) {
        $Key = self::getKey($key);
        $today = date("Ymd");
        $yesterday=date("Ymd", time() -60*60*24);
        $tomorrow = date("Ymd", time() + 60*60*12);
        if ($key==$tomorrow) return true;
        if ($Key==$yesterday) return true;
        return ($Key== $today);
    }

    static function getKey($key)
    {
        $currentKey = self::reverseMap($key);
        $key = substr($currentKey, 0, 4) . substr($currentKey, 5, 2) . substr($currentKey, 8, 2);
        return $key;
    }

    /*Reverses a complete word*/
    static function reverseMap($word)
    {
        $temp = "";
        foreach (str_split($word) as $letter) {
            $temp .= self::reverseMapping($letter);
        }
        return $temp;
    }

    /*
    Gets reversed mapped key for a letter
    */
     static function reverseMapping($letter)
    {
    $currentPos = strpos( utilities::juggledDictionary , $letter);
    return substr(utilities::normalDictionary,$currentPos,1);
    }

}
?>
