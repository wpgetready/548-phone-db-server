<?php
/**
* 20190326: First iteration of phonesdb database access.
 */
require_once('aws.php'); //class to retrieve images from Amazon S3
 

class sqlite
{
    private $db;
    //returns a list of phones from one brand
    const SELECT_PHONE_BRAND ="select * from phone where id_brand=%s";
    //return one phone
    const SELECT_PHONE ="select phone.id,phone.name,id_brand,brand.name as brand,model  from phone,brand where phone.id_brand=brand.id and phone.id=%s";
    //return a list of phones from a search
    const SELECT_SEARCH_PHONE ="select phone.id,phone.name,id_brand,brand.name as brand,model  from phone,brand where phone.id_brand=brand.id and phone.name like '%%%s%%'";
    //return a specific diagram
    const SELECT_DIAGRAM ="select diagram.id,id_phone,filename,description,format,sizemb,brand.name as brand_name,phone.model from diagram,phone,brand where diagram.id_phone = phone.id and brand.id = phone.id_brand and diagram.id =%s";
    //return a list of diagrams/objects for a phone.
    const SELECT_DIAGRAMS_PHONE = "select * from diagram where id_phone=%s";//return a list

    function __construct()
    {
        $db_path = dirname(__DIR__) . DS. 'db' . DS .'phones.db';
        try {
            $this->db = new PDO("sqlite:$db_path");
        } catch (Exception $e) {
            echo "Error: ", $e->getMessage();
        }
    }

    //Get phone data, return json info
    function getPhone($phoneid) {
        $sql=sprintf (sqlite::SELECT_PHONE ,$phoneid);
        $qry=$this->db->query($sql);
        $phone = $qry->fetchObject( "phone"); //Convert result into an object
        return json_encode($phone);
    }

    //Search for phones looking for the name. If not found, return -1
    //Otherwise, return a phone list
    function getSearchPhone($searchFor) {
        $sql= sprintf(sqlite::SELECT_SEARCH_PHONE,$searchFor);
        $result = $this->returnList($sql);
        if ($result==null) $result=-1; //null list, return -1 meaning not found.
        return json_encode($result);
    }

    //get list of diagrams available for a phone model.
    function getPhoneDiagrams($phoneId){
        $sql= sprintf(sqlite::SELECT_DIAGRAMS_PHONE,$phoneId);
        $result = $this->returnList($sql);
        if ($result==null) $result=-1; //null list, return -1 meaning not found.
        return json_encode($result);
    }

    //this function is not used for json purpose (now)
    //We'll need to build the path to specific diagram
    function getDiagram($diagramId) {
        $sql =sprintf(sqlite::SELECT_DIAGRAM,$diagramId);
        $qry=$this->db->query($sql);
        return $qry->fetchObject("diagram");
    }

    function returnList($sql) {
        $list =null;
        $qry=$this->db->query($sql);
        while ($record = $qry->fetch(PDO::FETCH_ASSOC)) {
            $list[]=$record;
        }
        return ($list);
    }

    function returnRecord($sql) {
        $sql = sprintf($sql);
        $qry=$this->db->query($sql);
        $result = $qry->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    /**
     * Attempt to return the object from Amazon.
     * There are few scenarios to deal with: the object doesn't exist in db(easy), exist in db but not in Amazon , exist in db but is not an Amazon object (ex: a link)
     * Usually an idObject is a diagram, but it could also be an image,a video, url,link  or whatever we need. We'll need to refine this along we progress.
     * @param $idObject
     */
    function requestObject($idObject) {
        $obj=$this->getDiagram($idObject);

        if ($obj == null) {
            //process this error
            echo ("diagram/object does not exist in database");
            die();
            return;
        }
        //make the path to the diagram
        $brand =  $obj->brand_name;
        $model =$obj->model;
        $filename = $obj->filename;
        $stream = aws::get_S3_object($brand,$model,$filename);
        if ($stream==null) {
            //process the error
            echo("diagram/object does not exist on S3");
            die();
            return;
        }
        return base64_encode($stream);
    }
}

/**
 * Implements a diagram representation , more complet than the original record table, since it includes model and brand name.
 * Class diagram
 */
class diagram {
    public $id;
    public $id_phone;
    public $filename;
    public $description;
    public $format;
    public $sizemb;
    public $brand_name;
    public $model;
}

/**
 * Class objectResponse
 * the object is serialized (base64) to transfer it properly to the android app
 */
class objectResponse implements JsonSerializable {
    public $found;
    public $not_found_reason;
    public $object;

    function __construct($not_found_reason)
    {
        $this->found=false;
        $this->not_found_reason = $not_found_reason;
        $this->object = null;
    }

    function addObject($o) {
        $this->found=true;
        $this->not_found_reason="OK";
        $this->object =$o;
    }


    function jsonSerialize()
    {
     return ['found' => $this->found,
             'not_found_reason'=> $this->not_found_reason,
             'object'=> $this->object
     ];
    }
}

//Use query SELECT_PHONE to fill all the fields properly (brand belong to other table)
class phone implements JsonSerializable {
    public $id;
    public $name;
    public $id_brand;
    public $brand;
    public $model;

    function jsonSerialize() {
        return [
          'id'          => intval($this->id),
          'name'        => $this->name,
          'id_brand'    => intval($this->id_brand),
          'brand'       => $this->brand,
          'model'       => $this->model
        ];
    }
}

/**
 * Refereneces https://www.sitepoint.com/use-jsonserializable-interface/
 */
